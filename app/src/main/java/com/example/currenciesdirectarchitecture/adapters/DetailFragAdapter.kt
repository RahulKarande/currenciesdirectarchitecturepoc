package com.example.currenciesdirectarchitecture.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.currenciesdirectarchitecture.databinding.ListItemsBinding
import com.example.currenciesdirectarchitecture.model.DataResponseList
import com.example.currenciesdirectarchitecture.model.Movies

class DetailFragAdapter(context:Context,var list: List<DataResponseList>) : RecyclerView.Adapter<DetailFragAdapter.ViewHolder>(){


    class ViewHolder(val binding:ListItemsBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(item:DataResponseList){
            /*with(binding){
                 originalTitle.text =item.original_title
            }*/
            binding.item = item
            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemsBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.bind(list[position])
    }
}