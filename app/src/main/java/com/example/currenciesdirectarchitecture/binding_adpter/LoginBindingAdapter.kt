package com.example.currenciesdirectarchitecture.binding_adpter

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.databinding.InverseMethod
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.currenciesdirectarchitecture.R
import com.example.currenciesdirectarchitecture.utils.Validator
import com.example.currenciesdirectarchitecture.viewmodel.LoginViewModel


object LoginBindingAdapter {
    lateinit var activity: Fragment
    @InverseMethod("valueString")
    @JvmStatic
    fun setArrayToString(value: String?): String? {

        return value
    }


    @JvmStatic
    fun valueString(value: String): String? {
        return value
    }


    @InverseBindingAdapter(attribute = "textInputValidator")
    @JvmStatic
    fun getTextInput(editText: EditText): String? {

        return editText.text.toString()
    }

    @BindingAdapter("textInputValidator")
    @JvmStatic
    fun setTextInput(view: EditText, value: String?) {
        if (TextUtils.isEmpty(view.text.toString()))
        //when it`s  null it will handle the exception through this operator ?:
            view.setText(value ?: "")
    }


    @BindingAdapter("textInputValidate")
    @JvmStatic
    fun isValidateEmail(
        inputView: EditText,
        mLoginViewModel: LoginViewModel?
    ) {
        var isValidDetails: Boolean = false
        inputView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //activity = getInstanceActivity(inputView)
                if (inputView.id == R.id.textInputValidatorUserName) {
                    if (!Validator.isEmailValid(
                            s.toString()
                        )
                    ) {
                        inputView.setError("Invalid email address")
                    } else {
                        isValidDetails = true
                    }
                } else {
                    isValidDetails = false
                }
                if (inputView.id == R.id.textInputValidatorPassword) {
                    if (!Validator.isPasswordValid(
                            s.toString()
                        )
                    ) {
                        inputView.setError("Invalid password")
                    } else {
                        isValidDetails = true
                    }
                } else {
                    isValidDetails = false
                }
                /* if (userModel != null) {
                     if ((inputView.id == R.id.userName || isEmailValid(userModel!!.userName)) && (inputView.id == R.id.password || isPasswordValid(
                             userModel!!.password
                         ))
                     ) {
                         updateButtonObserve(true, mLoginViewModel)
                     }
                 }*/
                updateButtonObserve(
                    isValidDetails,
                    mLoginViewModel
                )
            }

        })
    }

    fun updateButtonObserve(isValid: Boolean, mLoginViewModel: LoginViewModel?) {
        if (mLoginViewModel != null) {
            if (isValid) {
                mLoginViewModel!!.isButtonEnable.set(true)
            } else {
                mLoginViewModel!!.isButtonEnable.set(false)
            }
        }

    }

    @BindingAdapter("textInputValidatorAttrChanged")
    @JvmStatic
    fun setInputListener(
        inputView: EditText,
        listener: InverseBindingListener
    ) {
        inputView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                /* if (inputView.id == R.id.textInputValidatorUserName) {
                     if (!isEmailValid(s.toString())) {
                         inputView.setError("Invalid email address")

                     }
                 } else if (inputView.id == R.id.textInputValidatorPassword) {
                     if (!isPasswordValid(s.toString())) {
                         inputView.setError("Invalid password")
                     }
                 }*/
            }
        })
        listener.onChange()
    }

    fun validateCredentials(emailID: String, password: String): LiveData<String> {
        val loginErrorMessage = MutableLiveData<String>()
        if (Validator.isEmailValid(
                emailID
            )
        ) {
            /* if(password.length<8 && !isPasswordValid(password)){
                 loginErrorMessage.value = "Invalid Password"
             }else{*/
            loginErrorMessage.value = "Successful Login"
            //}
        } else {
            loginErrorMessage.value = "Invalid Email"
        }

        return loginErrorMessage
    }





    fun getInstanceActivity(view: View): Fragment {
        var fragmentInFrame: Fragment? = null

       /* if (view.context is MainActivity) {
            fragmentInFrame = (view.context as MainActivity).getSupportFragmentManager()
                .findFragmentById(R.id.container_first) as Fragment
        }*/
        return fragmentInFrame!!
    }


    @SuppressLint("ResourceType", "NewApi")
    @BindingAdapter("context", "setBackgroundVariations")
    @JvmStatic
    fun setOnClick(
        view: Button, context: Context,
        clickable: Boolean
    ) {
        if (clickable) {
            view.setBackgroundColor(context.getColor(R.color.colorAccent));
        } else {
            view.setBackgroundColor(context.getColor(R.color.color_gray));
        }
    }
}