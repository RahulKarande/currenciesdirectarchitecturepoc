package com.example.currenciesdirectarchitecture.binding_adpter

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import androidx.databinding.BindingAdapter
import com.example.currenciesdirectarchitecture.R
import com.example.currenciesdirectarchitecture.utils.Validator
import com.example.currenciesdirectarchitecture.viewmodel.RegisterViewModel
import java.util.regex.Pattern

object RegistrationBindingAdapter {


    @BindingAdapter("textInputValidate")
    @JvmStatic
    fun isValidateDetails(
        inputView: EditText,
        mRegisterViewModel: RegisterViewModel?
    ) {
        var isValidDetails: Boolean = false
        inputView.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                /*if (inputView.id == R.id.userNameEdt) {
                    if (!Validator.isEmptyText(

                        )
                    ) {
                        inputView.setError("Username is mandetory")
                    } else {
                        isValidDetails = true
                    }
                } else {
                    isValidDetails = false
                }*/

                if (inputView.id == R.id.userEmailEdt) {
                    if (!Validator.isEmailValid(
                            s.toString()
                        )
                    ) {
                        inputView.setError("Invalid email address")
                    } else {
                        isValidDetails = true
                    }
                } else {
                    isValidDetails = false
                }
                if (inputView.id == R.id.userPasswordEdt) {
                    if (!Validator.isPasswordValid(
                            s.toString()
                        )
                    ) {
                        inputView.setError("Invalid password")
                    } else {
                        isValidDetails = true
                    }
                } else {
                    isValidDetails = false
                }
                if (inputView.id == R.id.userContactNumberEdt) {
                    if (!Validator.isContactNumberValid(
                            s.toString()
                        )
                    ) {
                        inputView.setError("Invalid Mobile Number")
                    } else {
                        isValidDetails = true
                    }
                } else {
                    isValidDetails = false
                }
                updateButtonObserve(
                    isValidDetails,
                    mRegisterViewModel
                )
            }

        })
    }

    fun updateButtonObserve(isValid: Boolean, mRegisterViewModel: RegisterViewModel?) {
        if (mRegisterViewModel != null) {
            if (isValid) {
                mRegisterViewModel!!.isButtonEnable.set(true)
            } else {
                mRegisterViewModel!!.isButtonEnable.set(false)
            }
        }

    }


    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }


    @SuppressLint("ResourceType", "NewApi")
    @BindingAdapter( "setBackgroundVariations")
    @JvmStatic
    fun setOnClick(
        view: Button,
        clickable: Boolean
    ) {
        if (clickable) {
            view.setBackgroundColor(view.context.getColor(R.color.colorAccent));
        } else {
            view.setBackgroundColor(view.context.getColor(R.color.color_gray));
        }
    }


}