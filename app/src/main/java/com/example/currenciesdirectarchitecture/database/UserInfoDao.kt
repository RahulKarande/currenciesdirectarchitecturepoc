package com.example.currenciesdirectarchitecture.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface UserInfoDao {

    @Insert
    fun insert(userInfo: UserInfo)

    @Query("SELECT * FROM userInfo_table")
    fun getUserInfo(): LiveData<List<UserInfo>>

    @Query("SELECT * FROM userInfo_table WHERE mailId=:username")
    fun getAuthUser(username: String): LiveData<UserInfo>
}
