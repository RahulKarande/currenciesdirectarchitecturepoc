package com.example.currenciesdirectarchitecture.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [UserInfo::class],version = 1)
abstract class UserInfoDataBase : RoomDatabase(){
    abstract fun userInfoDao():UserInfoDao

    companion object {


        private var instance: UserInfoDataBase? = null

        fun getInstance(context: Context): UserInfoDataBase? {
            if (instance == null) {
                synchronized(UserInfoDataBase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        UserInfoDataBase::class.java, "notes_database"
                    )
                        .fallbackToDestructiveMigration()
                            //addCallBack with override OnCreate and onOpen of database
//                            .addCallback(roomCallback)
                        .build()
                }
            }
            return instance
        }

        fun destroyInstance() {
            instance = null
        }


    }
}