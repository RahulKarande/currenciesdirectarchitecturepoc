package com.example.currenciesdirectarchitecture.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "userInfo_table")
data class UserInfo(
    var name : String,
    var mailId : String,
    var mobNmbr : String
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}