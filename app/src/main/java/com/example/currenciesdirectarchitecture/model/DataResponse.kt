package com.example.currenciesdirectarchitecture.model

data class DataResponse(var data:List<DataResponseList>?,var page: String?,var per_page:String?,var total:String?,var total_pages:String?)