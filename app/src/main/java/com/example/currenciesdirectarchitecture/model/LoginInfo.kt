package com.example.currenciesdirectarchitecture.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

    class LoginInfo : BaseObservable(){
        var userName : String=""
            @Bindable get() = field
            set(userName) {
                field = userName
                notifyPropertyChanged(BR.userName)
            }

        var password : String = ""
            @Bindable get() = field
            set(password) {
                field = password
                notifyPropertyChanged(BR.password)
            }




        /*var errorMailId : String? = null
        @BindingAdapter("errorEmail")
        fun setMailIdError(editText: EditText, errorMsg:String?){
           // editText.setError(errorMsg?:"Invalid Email")
           editText.error = "Invalid MailId"
        }*/


    }