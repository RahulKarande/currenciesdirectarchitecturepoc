package com.example.currenciesdirectarchitecture.model


import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR


class RegisterInfo : BaseObservable(){
    var userName : String?=null
        @Bindable get() = field
        set(userName) {
            field = userName
            notifyPropertyChanged(BR.userName)
        }

    var mailId : String?=null
        @Bindable get() = field
        set(mailId) {
            field = mailId
            notifyPropertyChanged(BR.mailId)
        }

    var password : String?=null
        @Bindable get() = field
        set(password) {
            field = password
            notifyPropertyChanged(BR.password)
        }

    var mobNmbr : String?=null
        @Bindable get() = field
        set(mobNmbr) {
            field = mobNmbr
            notifyPropertyChanged(BR.mobNmbr)
        }

}