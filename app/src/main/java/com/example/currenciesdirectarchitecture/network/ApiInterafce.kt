package com.example.currenciesdirectarchitecture.network

import com.example.currenciesdirectarchitecture.model.DataResponse
import com.example.currenciesdirectarchitecture.model.MovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterafce {

    @GET("movie/top_rated?")
    fun getTopRatedMovies(@Query("api_key") apiKey:String):Call<MovieResponse>

    @GET("movie/{id}")
    fun getMovieDetails(@Path("id") id: Int, @Query("api_key") apiKey: String):Call<MovieResponse>

    @GET("users/?per_page=12&amp;page=1")
    fun getEmployees(): Call<DataResponse>

}