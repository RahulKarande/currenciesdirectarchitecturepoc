package com.example.currenciesdirectarchitecture.repository

import androidx.lifecycle.MutableLiveData
import com.example.currenciesdirectarchitecture.model.DataResponse
import com.example.currenciesdirectarchitecture.model.DataResponseList
import com.example.currenciesdirectarchitecture.network.ApiClient
import com.example.currenciesdirectarchitecture.network.ApiInterafce
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NetworkRepo {
    var dataList: MutableLiveData<List<DataResponseList>> = MutableLiveData()

    fun getMovieDetails(): MutableLiveData<List<DataResponseList>> {
        var apiServices = ApiClient.client.create(ApiInterafce::class.java)
        apiServices.getEmployees().enqueue(object : Callback<DataResponse> {
            override fun onFailure(call: Call<DataResponse>, t: Throwable) {

            }

            override fun onResponse(call: Call<DataResponse>, response: Response<DataResponse>) {
                dataList.value = response.body()?.data
            }

        })
        return dataList
    }
}