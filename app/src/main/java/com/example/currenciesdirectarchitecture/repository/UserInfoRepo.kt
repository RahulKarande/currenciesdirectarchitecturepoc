package com.example.currenciesdirectarchitecture.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.example.currenciesdirectarchitecture.database.UserInfo
import com.example.currenciesdirectarchitecture.database.UserInfoDao
import com.example.currenciesdirectarchitecture.database.UserInfoDataBase

class UserInfoRepo(application: Application){
    private lateinit var userInfoDao: UserInfoDao
    private lateinit var userInfo : LiveData<List<UserInfo>>

    init {
        val dataBase:UserInfoDataBase = UserInfoDataBase.getInstance(application.applicationContext)!!
        userInfoDao = dataBase.userInfoDao()
        userInfo =userInfoDao.getUserInfo()
    }

    fun insert(userInfo: UserInfo){
        insertUserInfoAsyncTask(userInfoDao).execute(userInfo)
    }

    fun getAllUserInfo():LiveData<List<UserInfo>>{
        return userInfo
    }

    fun getAuthUserDetails(userName:String):LiveData<UserInfo>{
        return userInfoDao.getAuthUser(userName)
    }

    private class insertUserInfoAsyncTask(val userInfoDao: UserInfoDao):
        AsyncTask<UserInfo, Unit, Unit>(){
        override fun doInBackground(vararg params: UserInfo?) {
            userInfoDao.insert(params[0]!!)
        }

    }
}