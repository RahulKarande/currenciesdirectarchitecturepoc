package com.example.currenciesdirectarchitecture.utils

import android.text.TextUtils
import java.util.regex.Pattern

object Validator {

    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun isPasswordValid(password: String): Boolean {
        //val expression = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#\$%^&+=!])(?=\\\\S+\$).{4,}\$";
        val expression = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#\$%!\\-_?&])(?=\\S+\$).{8,}"
        val pattern = Pattern.matches(expression, password)
        return pattern
    }

    fun isContactNumberValid(number: String): Boolean {
        //val expression = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#\$%^&+=!])(?=\\\\S+\$).{4,}\$";
        val expression = "[0-9]{10}"
        val matcher = Pattern.matches(expression, number)
        return matcher
    }

    fun isEmptyText(value: String): Boolean {
        if (!TextUtils.isEmpty(value)) {
            return true
        }
        return false
    }
}