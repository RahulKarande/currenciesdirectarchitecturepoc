package com.example.currenciesdirectarchitecture.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.currenciesdirectarchitecture.R
import com.example.currenciesdirectarchitecture.adapters.DetailFragAdapter
import com.example.currenciesdirectarchitecture.databinding.DetailFragLayoutBinding
import com.example.currenciesdirectarchitecture.databinding.RegisterFragmentBinding
import com.example.currenciesdirectarchitecture.model.DataResponseList
import com.example.currenciesdirectarchitecture.model.Movies
import com.example.currenciesdirectarchitecture.network.ApiClient
import com.example.currenciesdirectarchitecture.network.ApiInterafce
import com.example.currenciesdirectarchitecture.viewmodel.DetailFragViewModel
import com.example.currenciesdirectarchitecture.viewmodel.RegisterViewModel

class DetailFragment : Fragment() {
    lateinit var detailFragView: View
    lateinit var detailFragViewModel: DetailFragViewModel
    lateinit var detailFragAdapter: DetailFragAdapter
    lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val detailFRagBinder: DetailFragLayoutBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.detail_frag_layout, container, false
        )
        detailFragView = detailFRagBinder.root
        recyclerView = detailFRagBinder.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(context)


        return detailFragView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailFragViewModel = ViewModelProviders.of(activity!!).get(DetailFragViewModel::class.java)


        detailFragViewModel.getDetailsFromApi()
            .observe(this, object : Observer<List<DataResponseList>> {
                override fun onChanged(t: List<DataResponseList>?) {
                    detailFragAdapter = t?.let { DetailFragAdapter(activity!!, it) }!!
                    recyclerView.adapter = detailFragAdapter
                }

            })


    }
}