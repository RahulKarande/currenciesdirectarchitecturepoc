package com.example.currenciesdirectarchitecture.view.fragments


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.example.currenciesdirectarchitecture.MainActivity
import com.example.currenciesdirectarchitecture.R
import com.example.currenciesdirectarchitecture.databinding.LoginFragmentBinding
import com.example.currenciesdirectarchitecture.model.LoginInfo
import com.example.currenciesdirectarchitecture.viewmodel.LoginViewModel


class LoginFragment : Fragment() {

    lateinit var loginView: View
    lateinit var loginViewModel: LoginViewModel
    lateinit var loginInfo: LoginInfo;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val loginBinder: LoginFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.login_data_binding, container, false)
        loginBinder.setLifecycleOwner(activity)
        loginInfo = LoginInfo()
        loginBinder.loginViewModel = loginViewModel
        loginView = loginBinder.root
        loginInfo.userName = loginBinder.textInputValidatorUserName.text.toString()
        loginInfo.password = loginBinder.textInputValidatorPassword.text.toString()
        loginViewModel.loginInfo.value=loginInfo

        loginBinder.registerBtn.setOnClickListener(View.OnClickListener {
            Navigation.findNavController(view!!)
                .navigate(R.id.action_loginFragment_to_registerFragment);
         /*   val action =
                LoginFragmentDirections
                    .actionLoginFragmentToRegisterFragment()
            view!!.findNavController().navigate(action)
*/
            /*var mainActivity: MainActivity = activity as MainActivity
            mainActivity.replaceFrag(RegisterFragment())*/
        })
        return loginView
    }
}