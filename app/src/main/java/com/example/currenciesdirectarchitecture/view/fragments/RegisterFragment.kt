package com.example.currenciesdirectarchitecture.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.currenciesdirectarchitecture.R
import com.example.currenciesdirectarchitecture.database.UserInfo
import com.example.currenciesdirectarchitecture.databinding.RegisterFragmentBinding
import com.example.currenciesdirectarchitecture.viewmodel.RegisterViewModel


class RegisterFragment : Fragment() {
    lateinit var registerView: View
    lateinit var registerViewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val registerBinder: RegisterFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.register_fragment_layout, container, false
        )
        registerView = registerBinder.root

        registerBinder.viewModel=registerViewModel
        registerBinder.registrBtn.setOnClickListener(View.OnClickListener {
            Toast.makeText(activity, "User Registered", Toast.LENGTH_LONG).show()
            val userInfo = UserInfo(
                registerBinder.userNameEdt.text.toString(),
                registerBinder.userEmailEdt.text.toString(),
                registerBinder.userContactNumberEdt.text.toString()
            )
            registerViewModel.insert(userInfo)
            Navigation.findNavController(view!!)
                .navigate(R.id.action_registerFragment_to_loginFragment);

 })
        return registerView
    }

}