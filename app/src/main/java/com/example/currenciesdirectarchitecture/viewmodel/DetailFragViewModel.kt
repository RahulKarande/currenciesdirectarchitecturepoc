package com.example.currenciesdirectarchitecture.viewmodel

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.currenciesdirectarchitecture.model.DataResponse
import com.example.currenciesdirectarchitecture.model.DataResponseList
import com.example.currenciesdirectarchitecture.model.MovieResponse
import com.example.currenciesdirectarchitecture.model.Movies
import com.example.currenciesdirectarchitecture.network.ApiClient
import com.example.currenciesdirectarchitecture.network.ApiInterafce
import com.example.currenciesdirectarchitecture.repository.NetworkRepo
import retrofit2.Call
import retrofit2.Response
import retrofit2.Callback


class DetailFragViewModel : AndroidViewModel {
    lateinit var networkRepo:NetworkRepo

    constructor(application: Application) : super(application) {
         networkRepo = NetworkRepo()
    }

    fun getDetailsFromApi() :MutableLiveData<List<DataResponseList>>{
        return networkRepo.getMovieDetails()
    }


}