package com.example.currenciesdirectarchitecture.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.databinding.ObservableField


import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import com.example.currenciesdirectarchitecture.R
import com.example.currenciesdirectarchitecture.database.UserInfo
import com.example.currenciesdirectarchitecture.model.LoginInfo
import com.example.currenciesdirectarchitecture.repository.UserInfoRepo
import com.example.currenciesdirectarchitecture.repository.ValidationRepository
import com.example.currenciesdirectarchitecture.view.fragments.LoginFragmentDirections


class LoginViewModel : AndroidViewModel {
    var validationRepository: ValidationRepository
    var isButtonEnable = ObservableField<Boolean>(false)
    var loginInfo: MutableLiveData<LoginInfo>
    var userInfoRepo: UserInfoRepo
    var allUserInfo: LiveData<List<UserInfo>>


    init {
        loginInfo = MutableLiveData()
    }

    constructor(application: Application) : super(application) {

        validationRepository = ValidationRepository(application)
        userInfoRepo = UserInfoRepo(application)
        allUserInfo = userInfoRepo.getAllUserInfo()
    }


    fun validateCredentials(email: String, passWord: String): LiveData<String> {
        return validationRepository.validateCredentials(email, passWord)
    }

    fun onClick(view: View, userName: String?) {
        if (userInfoRepo.getAuthUserDetails(userName!!) != null) {
            val action =
                LoginFragmentDirections
                    .actionLoginFragmentToDetailFragment()
            view!!.findNavController().navigate(action)

            Toast.makeText(view.context, "LoginSuccess for $userName", Toast.LENGTH_SHORT).show()
        }
    }

    fun getAllUserDetails(): LiveData<List<UserInfo>> {
        return allUserInfo
    }


}