package com.example.currenciesdirectarchitecture.viewmodel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.currenciesdirectarchitecture.database.UserInfo
import com.example.currenciesdirectarchitecture.repository.UserInfoRepo
import com.example.currenciesdirectarchitecture.repository.ValidationRepository

class RegisterViewModel(application: Application): AndroidViewModel(application){

    private var validationRepo: ValidationRepository = ValidationRepository(application)
    var isButtonEnable = ObservableField<Boolean>(false)


    /*fun validateUserDetails(userName:String,mailId:String,password:String,mobNo:String):LiveData<String>{
    }*/

    private var userInfoRepo: UserInfoRepo = UserInfoRepo(application)
    private var allUserInfo: LiveData<List<UserInfo>> = userInfoRepo.getAllUserInfo()
    fun insert(userInfo: UserInfo) {
        userInfoRepo.insert(userInfo)
    }

    fun getAllUserInfo():LiveData<List<UserInfo>>{
        return  allUserInfo
    }

    override fun onCleared() {
        super.onCleared()
    }
}